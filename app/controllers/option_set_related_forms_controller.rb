class OptionSetRelatedFormsController < ApplicationController
  include StandardImportable
  include Parameters

  decorates_assigned :forms

  def new
    authorize!(:create, OptionSets::RelatedForm)
    build_object

    @related_forms = Form.where(:mission => current_mission).published.by_name
  end

  def create
    authorize!(:create, OptionSets::RelatedForm)

    related_form = OptionSets::RelatedForm.new(mission: current_mission)
    related_form.related_form_id = params[:option_sets_related_form][:related_form_id]
    related_form.name = params[:option_sets_related_form][:name]

    if related_form.valid?
      related_form.save
      render(json: {:a => 2})
      flash[:success] = t("option_set_related_form.create_success")
    else
      # render where we should redirect
      render(json: {:a => 1})
    end
  end

  protected

  def build_object
    @option_set_related_form = OptionSets::RelatedForm.new(mission: current_mission)
  end
end
