class LdapGroup < ActiveLdap::Base
  ldap_mapping :dn_attribute => "cn",
               :prefix => "ou=groups"
  has_many :members,  :wrap => "member", :class_name => "LdapUser", :primary_key => 'dn'
  has_many :owners,  :wrap => "owner", :class_name => "LdapUser",  :primary_key => 'dn'



  #returns related mission
  def mission
    Mission.find_by_ldap_dn(self.dn.to_s) or self.create_mission_from_ldap
  end

  def create_mission_from_ldap
    Mission.create(
        :name => self.cn,
        :ldap_dn => self.dn.to_s
    )
  end
end
