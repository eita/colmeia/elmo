class LdapConnect

  attr_reader :ldap

  def initialize(params = {})
    ldap_config = YAML.load_file("#{RAILS_ROOT}/config/ldap.yml")[RAILS_ENV]
    ldap_options = params
    ldap_options[:encryption] = :simple_tls if ldap_config["ssl"]

    @ldap = Net::LDAP.new(ldap_options)
    @ldap.host = ldap_config["host"]
    @ldap.port = ldap_config["port"]
    @ldap.base = ldap_config["base"]
    @ldap.auth ldap_config["admin_user"], ldap_config["admin_password"] if params[:admin]
  end

end