class LdapUser < ActiveLdap::Base
  ldap_mapping :dn_attribute => "cn",
               :prefix => "ou=users"
  belongs_to :groups, :class_name => 'LdapGroup', :many => 'member', :foreign_key => 'dn'
  belongs_to :groups_as_owner, :class_name => 'LdapGroup', :many => 'owner', :foreign_key => 'dn'
end