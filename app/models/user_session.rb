class UserSession < Authlogic::Session::Base
  # only session related configuration goes here, see documentation for sub modules of Authlogic::Session
  # other config goes in acts_as block
  logout_on_timeout(true)
  allow_http_basic_auth(false) # We handle our own basic auth
  httponly(true)
  secure(Rails.env.production?)
  find_by_login_method :find_or_create_from_ldap
  verify_password_method :valid_ldap_or_db_credentials?


  # override find() to eager load User.assignments
  def self.find(*args)
    with_scope(find_options: User.includes(:assignments)) do
      super
    end
  end

  def to_key
    new_record? ? nil : [ self.send(self.class.primary_key) ]
  end
end
