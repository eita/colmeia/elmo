# frozen_string_literal: true

module OptionSets

  # Creates an optionset with values that come from a related form
  class RelatedForm
    include ActiveModel::Model


    attr_accessor :option_set, :mission, :name, :related_form_id


    validates :name, presence: true
    validates :related_form_id, presence: true

    def save
      OptionSet.transaction do
        #option set creation
        option_set = OptionSet.new
        option_set.mission_id = self.mission.id
        option_set.name = self.name
        option_set.form_id = self.related_form_id
        option_set.root_node = OptionNode.new
        option_set.geographic = false
        option_set.allow_coordinates = false

        option_set.save

        self.option_set = option_set

        #import form responses
        form = Form.find(self.related_form_id)
        qing = form.first_key_qing

        row_idx = 0
        responses = Response.where(:form_id => self.related_form_id, :reviewed => true).all
        responses.each do |response|
          option = response.create_option(qing, self.option_set, row_idx)
          row_idx += 1
        end
      end
    end
  end
end