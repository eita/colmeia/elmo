class ELMO.Views.RelatedFormView extends ELMO.Views.ApplicationView

  el: 'form#new_option_sets_related_form'

  initialize: (options) ->
    console.log('initialized2')

  events:
    'submit': 'formSubmitted'

  formSubmitted: (evt) ->

    data = @$el.serialize()


    $.ajax
      url: ELMO.app.url_builder.build('option-set-related-forms')
      data: data
      method: "post"
      success: =>
        window.location.href = ELMO.app.url_builder.build('option-sets')
        console.log('returned')
      error: =>
        alert('error when creating option set')

    return false