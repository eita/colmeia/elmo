class AddResponseIdToOption < ActiveRecord::Migration[5.2]
  def change
    add_column :options, :response_id, :uuid, index: true
    add_foreign_key :options, :responses, column: :response_id
  end
end
