class AddLdapFieldsToUserAndMission < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :ldap_dn, :string
    add_column :missions, :ldap_dn, :string
    add_index :users, :ldap_dn
    add_index :missions, :ldap_dn
  end
end
