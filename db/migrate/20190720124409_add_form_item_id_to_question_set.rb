class AddFormItemIdToQuestionSet < ActiveRecord::Migration[5.2]
  def change
    add_column :option_sets, :form_id, :uuid, index: true
    add_foreign_key :option_sets, :forms, column: :form_id
  end
end
